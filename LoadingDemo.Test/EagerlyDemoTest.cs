﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoadingDemo.Test
{
    [TestClass]
    public class EagerlyDemoTest
    {
        [TestMethod]
        public void EagerlyLoad_SingleLevel()
        {
            using (var context = new BlogDBEntities())
            {
                // load a blog
                var blogs1 = context.Blogs.Where(x=>x.BlogID==1).FirstOrDefault();
                //see the generated script
                /*
                SELECT TOP (1) 
                    [Extent1].[BlogID] AS [BlogID], 
                    [Extent1].[Title] AS [Title], 
                    [Extent1].[OwnerID] AS [OwnerID], 
                    [Extent1].[CreatedDate] AS [CreatedDate]
                    FROM [dbo].[Blog] AS [Extent1]
                    WHERE 1 = [Extent1].[BlogID]
                */
                //load a blog with its posts
                var blog2 = context.Blogs.Where(x => x.BlogID == 2).Include(x=>x.Posts).FirstOrDefault();
                //see the generated script
                
                /*
                SELECT 
                    [Project1].[BlogID] AS [BlogID], 
                    [Project1].[Title] AS [Title], 
                    [Project1].[OwnerID] AS [OwnerID], 
                    [Project1].[CreatedDate] AS [CreatedDate], 
                    [Project1].[C1] AS [C1], 
                    [Project1].[PostID] AS [PostID], 
                    [Project1].[BlogID1] AS [BlogID1], 
                    [Project1].[Tittle] AS [Tittle], 
                    [Project1].[CreatedDate1] AS [CreatedDate1]
                    FROM ( SELECT 
                        [Limit1].[BlogID] AS [BlogID], 
                        [Limit1].[Title] AS [Title], 
                        [Limit1].[OwnerID] AS [OwnerID], 
                        [Limit1].[CreatedDate] AS [CreatedDate], 
                        [Extent2].[PostID] AS [PostID], 
                        [Extent2].[BlogID] AS [BlogID1], 
                        [Extent2].[Tittle] AS [Tittle], 
                        [Extent2].[CreatedDate] AS [CreatedDate1], 
                        CASE WHEN ([Extent2].[PostID] IS NULL) THEN CAST(NULL AS int) ELSE 1 END AS [C1]
                        FROM   (SELECT TOP (1) [Extent1].[BlogID] AS [BlogID], [Extent1].[Title] AS [Title], [Extent1].[OwnerID] AS [OwnerID], [Extent1].[CreatedDate] AS [CreatedDate]
                            FROM [dbo].[Blog] AS [Extent1]
                            WHERE 2 = [Extent1].[BlogID] ) AS [Limit1]
                        LEFT OUTER JOIN [dbo].[Post] AS [Extent2] ON [Limit1].[BlogID] = [Extent2].[BlogID]
                    )  AS [Project1]
                    ORDER BY [Project1].[BlogID] ASC, [Project1].[C1] ASC
                */

            }
        }
    }
}
