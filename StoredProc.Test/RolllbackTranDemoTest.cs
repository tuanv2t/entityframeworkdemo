﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StoredProc.Test
{
    [TestClass]
    public class RolllbackTranDemoTest
    {
        [TestMethod]
        public void RolllbackTranDemo_Test()
        {
            try
            {
                using (var context = new BlogDBEntities())
                {
                    context.RolllbackTranDemo();//there will be an exception :(

                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
           
        }

        [TestMethod]
        public void RolllbackTranDemo_LINQ_DBML_Test()
        {
            try
            {
                using (var context = new DataClasses1DataContext())
                {
                    context.RolllbackTranDemo();//LINQ DBML works cool :D

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        [TestMethod]
        public void RolllbackTranDemo_CdodeFirst_Test()
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["BlogDBConnectionString"].ConnectionString;
                using (var context = new BlogDBContext(connectionString))
                {
                    context.RolllbackTranDemo();//there will be an exception :(

                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

    }
}
