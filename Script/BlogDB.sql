﻿CREATE TABLE Blog (
	BlogID INT IDENTITY(1, 1)
	,Title NVARCHAR(500)
	,OwnerID INT NOT NULL
	,CreatedDate DATETIME
	,PRIMARY KEY (BlogID)
	)
GO

CREATE TABLE Post (
	PostID INT IDENTITY(1, 1)
	,BlogID INT NOT NULL
	,Tittle NVARCHAR(400)
	,CreatedDate DATETIME
	,PRIMARY KEY (PostID)
	,CONSTRAINT [FK_Post_Blog] FOREIGN KEY ([BlogID]) REFERENCES [dbo].[Blog]([BlogID])
	)
GO

CREATE TABLE Comment (
	CommentID INT IDENTITY(1, 1)
	,PostID INT NOT NULL
	,CommentDate DATETIME
	,Content NVARCHAR(MAX)
	,PRIMARY KEY (CommentID)
	,CONSTRAINT [FK_Comment_Post] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Post]([PostID])
	)
GO

IF NOT EXISTS (
		SELECT *
		FROM Blog
		WHERE BlogID = 1
		)
BEGIN
	INSERT INTO Blog (
		Title
		,OwnerID
		,CreatedDate
		)
	VALUES (
		N'Blog ABC 001'
		,1
		,'06/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Blog
		WHERE BlogID = 2
		)
BEGIN
	INSERT INTO Blog (
		Title
		,OwnerID
		,CreatedDate
		)
	VALUES (
		N'Blog DEF 002'
		,1
		,'05/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Blog
		WHERE BlogID = 3
		)
BEGIN
	INSERT INTO Blog (
		Title
		,OwnerID
		,CreatedDate
		)
	VALUES (
		N'Blog GHI 003'
		,2
		,'04/30/2016'
		)
END
GO
-- Init Post
IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 1
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		1
		,N'Post ABC 001'
		,'06/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 2
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		1
		,N'Post ABC 002'
		,'05/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 3
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		2
		,N'Post DEF 001'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 4
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		2
		,N'Post DEF 002'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 5
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		3
		,N'Post GHI 001'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Post
		WHERE PostID = 6
		)
BEGIN
	INSERT INTO Post (
		BlogID
		,Tittle
		,CreatedDate
		)
	VALUES (
		3
		,N'Post GHI 002'
		,'04/30/2016'
		)
END
GO
-- Init Comment
IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 1
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		1
		,N'Comment ABC 001 - comment1'
		,'06/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 2
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		1
		,N'Comment ABC 001 - comment2'
		,'05/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 3
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		1
		,N'Comment ABC 001 - comment3'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 4
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		2
		,N'Comment ABC 002 - comment1'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 5
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		2
		,N'Comment ABC 002 - comment2'
		,'04/30/2016'
		)
END
GO

IF NOT EXISTS (
		SELECT *
		FROM Comment
		WHERE CommentID = 6
		)
BEGIN
	INSERT INTO Comment (
		PostID
		,Content
		,CommentDate
		)
	VALUES (
		2
		,N'Comment ABC 002 - comment3'
		,'04/30/2016'
		)
END

GO
CREATE PROCEDURE [dbo].[RolllbackTranDemo]
AS
BEGIN
	
	BEGIN TRAN T1
	IF @@trancount > 0
				ROLLBACK TRANSACTION T1

	
END