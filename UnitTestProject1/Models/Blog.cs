﻿using System;
using System.Collections.Generic;

namespace UnitTestProject1.Models
{
    public partial class Blog
    {
        public Blog()
        {
            Post = new HashSet<Post>();
        }

        public int BlogId { get; set; }
        public string Title { get; set; }
        public int OwnerId { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual ICollection<Post> Post { get; set; }
    }
}
