﻿using System;
using System.Collections.Generic;

namespace UnitTestProject1.Models
{
    public partial class Comment
    {
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public DateTime? CommentDate { get; set; }
        public string Content { get; set; }

        public virtual Post Post { get; set; }
    }
}
